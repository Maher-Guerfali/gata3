const mongoose = require('mongoose');

const BetSchema = new mongoose.Schema(
  {
    Amount: {
      type: String,
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model('Bet', BetSchema);

const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
  {
    Email: {
      type: String,
    },
    Password: {
      type: String,
    },
    Username: {
      type: String,
    },
    Rank: {
      type: String,
    },
    RankingPoints: {
      type: String,
    },
    Image: {
      type: String,
    },
    Role: {
      type: String,
    },

    Bibliotheque: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Bibliotheque',
    },
    Wallet: {
      type: String,
    },
    Matchs: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Match',
      },
    ],
    Bets: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Bet',
      },
    ],
  },
  { timestamps: true }
);
module.exports = mongoose.model('User', UserSchema);

const mongoose = require('mongoose');

const AchatSchema = new mongoose.Schema(
  {
    Supply: {
      type: String,
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model('Achat', AchatSchema);

const mongoose = require('mongoose');

const MatchSchema = new mongoose.Schema(
  {
    Time: {
      type: String,
    },
    TimeLimit: {
      type: String,
    },
    Map: {
      type: String,
    },
    Price: {
      type: String,
    },
    Bets: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Bet',
      },
    ],
  },
  { timestamps: true }
);
module.exports = mongoose.model('Match', MatchSchema);

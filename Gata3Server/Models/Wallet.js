const mongoose = require('mongoose');

const WalletSchema = new mongoose.Schema(
  {
    Amount: {
      type: String,
    },
    Achats: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Achat',
      },
    ],
  },
  { timestamps: true }
);
module.exports = mongoose.model('Wallet', WalletSchema);

const mongoose = require('mongoose');

const BibliothequeSchema = new mongoose.Schema(
  {
    Skins: [
      {
        type: String,
      },
    ],
    Champions: [
      {
        type: String,
      },
    ],
  },
  { timestamps: true }
);
module.exports = mongoose.model('Bibliotheque', BibliothequeSchema);

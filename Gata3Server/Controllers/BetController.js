const Bet = require('../Models/Bet');
const User = require('../Models/User');

const index = async (req, res, next) => {
  try {
    const bets = await Bet.find();
    if (!bets) {
      res.status(404).json('no achat found');
    } else {
      res.status(200).json(bets);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const findOne = async (req, res, next) => {
  try {
    const betId = req.params.id;
    const bet = await bet.findById(betId);
    if (!bet) {
      res.status(404).json('bet not found');
    } else {
      res.status(200).json(bet);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
const add = async (req, res, next) => {
  try {
    const data = req.body;
    const user = await User.findById(data.user);
    if (!user) {
      res.status(400).json('wrong user data');
    }
    const newBet = new Bet({
      Amount: data.Amount,
    });

    const savedBet = await newBet.save();
    user.Bets.push(savedBet);
    const savedUser = await user.save();
    console.log(savedUser);
    res.status(200).json(savedBet);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

module.exports = {
  index,
  findOne,
  add,
};

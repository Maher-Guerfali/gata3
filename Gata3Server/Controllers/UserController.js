const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const Client = require('../Models/User');

exports.Register = async function (req, res) {
  try {
    //checking if email already in use
    const emailExist = await Client.findOne({
      Email: req.body.Email,
    });
    if (emailExist) return res.status(400).send('email already in use');

    //hashing password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.Password, salt);

    //creating user
    const client = new Client({
      Username: req.body.Username,
      Email: req.body.Email,
      Password: hashedPassword,
      Wallet: '0',
    });

    const savedclient = await client.save();

    res.status(200).json({
      client: savedclient,
      status: true,
      message: 'You successfully created an account',
    });
    console.log(savedclient);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
};

exports.Login = async function (req, res) {
  try {
    //checking username
    const ClientExist = await Client.findOne({
      Email: req.body.Email,
    });
    if (!ClientExist)
      return res.status(404).json({
        message: 'Client not found',
        status: false,
      });

    //checking password
    const passwordValidbcrypt = await bcrypt.compare(
      req.body.Password,
      ClientExist.Password
    );
    if (!passwordValidbcrypt)
      return res.status(400).json({
        message: 'Wrong Password',
        status: false,
      });

    //Creating and assigning token
    const token = jwt.sign(
      {
        ClientExist,
      },
      process.env.TOKEN_SECRET,
      {
        expiresIn: '30d',
      }
    );
    res.status(200).json({
      message: 'you are logged in',
      Client: ClientExist,
      token,
    });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const Biblio = require('../Models/Bibliotheque');

const findOne = async (req, res, next) => {
  try {
    const biblioId = req.params.id;
    const biblio = await Biblio.findById(biblioId);
    if (!biblio) {
      res.status(404).json('biblio not found');
    } else {
      res.status(200).json(biblio);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
const add = async (req, res, next) => {
  try {
    const data = req.body;
    const user = await User.findById(data.user);
    if (!user) {
      res.status(400).json('wrong user data');
    }
    const newBiblio = new Biblio();

    const savedBib = await newBiblio.save();
    user.Bibliotheque = savedBib;
    const savedUser = await user.save();
    console.log(savedUser);
    res.status(200).json(savedBib);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const update = async (req, res) => {
  try {
    const data = req.body;
    const bibId = req.params.id;
    const bib = await Biblio.findById(bibId);
    if (!bib) {
      res.status(404).json('bib not found ');
    } else {
      if (data.skin) {
        bib.Skins.push(data.skin);
      }
      if (data.champion) {
        bib.Champions.push(data.champion);
      }

      const savedBib = await bib.save();
      res.status(200).json(savedBib);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const removeFromBib = async (req, res) => {
  try {
    const data = req.body;
    const bibId = req.params.id;
    const bib = await Biblio.findById(bibId);
    if (!bib) {
      res.status(404).json('bib not found ');
    } else {
      if (data.skin) {
        bib.Skins = bib.Skins.filter(function (value, index, arr) {
          return value != data.skin;
        });
      }
      if (data.champion) {
        bib.Champions = bib.Champions.filter(function (value, index, arr) {
          return value != data.champion;
        });
      }

      const savedBib = await bib.save();
      res.status(200).json(savedBib);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

module.exports = {
  index,
  findOne,
  add,
  update,
  removeFromBib,
};

const Achat = require('../Models/Achats');
const Wallet = require('../Models/Wallet');

const index = async (req, res, next) => {
  try {
    const achats = await Achat.find();
    if (!achats) {
      res.status(404).json('no achat found');
    } else {
      res.status(200).json(achats);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const findOne = async (req, res, next) => {
  try {
    const achatID = req.params.id;
    const achat = await Achat.findById(achatID);
    if (!achat) {
      res.status(404).json('achat not found');
    } else {
      res.status(200).json(achat);
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
const add = async (req, res, next) => {
  try {
    const data = req.body;
    const wallet = await Wallet.findById(data.wallet);
    if (!wallet) {
      res.status(400).json('wrong wallet data');
    }
    const newAchat = new Achat({
      Supply: data.Supply,
    });
    const savedAchat = await newAchat.save();
    wallet.Achats.push(savedAchat);
    const savedWallet = await wallet.save();
    console.log(savedWallet);
    res.status(200).json(savedAchat);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

module.exports = {
  index,
  findOne,
  add,
};

using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PPM : MonoBehaviour
{
    [SerializeField]
     float mouseXSensitivity, sprintSpeed , walkSpeed, smoothTime;
    float verticalLookRotation;
    [SerializeField] GameObject cameraHolder;
    Rigidbody rb;
    Vector3 moveAmount;
    Vector3 smoothMoveVelocity;
    private Animator anim;
    public float rayhit;
    RawImage item;
    [SerializeField] GameObject cursor;

    int i;
    public bool haskey;
    Texture2D key;

    private GameObject gameobject;

     Animator anime;
    GameObject garn;
    GameObject garn1;
    GameObject tableau;
    GameObject tableau1;
    GameObject tableau2;
    GameObject zarbiya;
    GameObject korsi;


    private PhotonView pv;

    public Animator pull_01;
    public Animator pull_02;
    public bool open;
    public bool sabela1;
    public bool sabela2;
    [SerializeField]
    public ParticleSystem water;
    [SerializeField]
    public ParticleSystem water2;
    [SerializeField]
    public ParticleSystem water3;
    private float inc;
    [SerializeField] float rotationSpeed = 300f;
    [SerializeField] float translationSpeed = 30f;
    bool dragging = false;

    private void Awake()
    {
    
    }

    // Start is called before the first frame update
    private void Start()
    {
        sabela1 = false;
        pv = GetComponent<PhotonView>();
        rb = GetComponent<Rigidbody>();

        anim = GetComponent<Animator>();
        if (GameObject.Find("WaterLeak"))
        {
            Debug.Log("yatik asba ya samer");
        water = GameObject.Find("WaterLeak").GetComponent<ParticleSystem>();
        water2 = GameObject.Find("WaterLeak 1").GetComponent<ParticleSystem>();
        water3 = GameObject.Find("WaterLeak 2").GetComponent<ParticleSystem>();
        }
      
        if (GameObject.FindGameObjectWithTag("garn"))
        {
            garn = GameObject.FindGameObjectWithTag("garn");
        }
        if (GameObject.FindGameObjectWithTag("garn1"))
        {
            garn1 = GameObject.FindGameObjectWithTag("garn1");
        }
        if (GameObject.FindGameObjectWithTag("tableau"))
        {
            tableau = GameObject.FindGameObjectWithTag("tableau");
        }
        if (GameObject.FindGameObjectWithTag("tableau1"))
        {
            tableau1 = GameObject.FindGameObjectWithTag("tableau1");
        }
        if (GameObject.FindGameObjectWithTag("tableau2"))
        {
            tableau2 = GameObject.FindGameObjectWithTag("tableau2");
        }
        if (GameObject.FindGameObjectWithTag("zarbiya"))
        {
            zarbiya = GameObject.FindGameObjectWithTag("zarbiya");
        }
        if (GameObject.FindGameObjectWithTag("korsi"))
        {
            korsi = GameObject.FindGameObjectWithTag("korsi");
        }

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        i = 0;
        if (GameObject.FindGameObjectWithTag("Item"))
        {
            item = GameObject.FindGameObjectWithTag("Item").GetComponent<RawImage>();
        }
     
        haskey = false;
        key = Resources.Load<Texture2D>("Key");
        inc = 3;
        Debug.Log(inc);
        if (water)
        {
            Debug.Log("yatik asba ya iyed");
            water.Stop();
            water2.Stop();
            water3.Stop();
        }
       
       


        sabela2 = false;
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.SerializationRate = 5;
        PhotonNetwork.SendRate = 20;
      
    }

    // Update is called once per frame
    void Update()
    {
        if (pv.IsMine)
        {
            Look();
            Move();

        }
        else
        {
            cameraHolder.gameObject.SetActive(false);
        }


      
    }

    void Look()
    {

        transform.Rotate(Vector3.up * Input.GetAxisRaw("Mouse X") * mouseXSensitivity);
        verticalLookRotation += Input.GetAxisRaw("Mouse Y") * mouseXSensitivity;
        verticalLookRotation = Mathf.Clamp(verticalLookRotation, -90f, 90f);
        cameraHolder.transform.localEulerAngles = Vector3.left * verticalLookRotation;
        if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
        }
        else if (Input.GetKey(KeyCode.Mouse0))
        {
            dragging = true;
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            cameraHolder.GetComponentInChildren<Camera>().fieldOfView-=3;

        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            cameraHolder.GetComponentInChildren<Camera>().fieldOfView+=3;

        }
        if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            cameraHolder.GetComponentInChildren<Camera>().fieldOfView = 60;

        }
        RaycastHit hit;
        Debug.DrawRay(cursor.transform.position, cursor.transform.forward * rayhit, Color.red);
        if (Physics.Raycast(cursor.transform.position, cursor.transform.forward, out hit, rayhit))
        {
        
            
            if (hit.transform.tag == "garn")
            {
                Debug.Log(dragging);
                if (dragging)
                {
                    float x = Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
                  
                    pv.RPC("Mgarn",RpcTarget.All,x.ToString());
                }
            }
            else if (hit.transform.tag == "garn1")
            {
                Debug.Log(dragging);
                if (dragging)
                {
                    float x = Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
                    pv.RPC("Mgarn1", RpcTarget.All, x.ToString());
                }
            }
            else if (hit.transform.tag == "tableau")
            {
                Debug.Log(dragging);
                if (dragging)
                {
                    float x = Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
                    pv.RPC("Mtableau", RpcTarget.All, x.ToString());
                }
            }
            else if (hit.transform.tag == "tableau1")
            {
                Debug.Log(dragging);
                if (dragging)
                {
                    float x = Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
                    pv.RPC("Mtableau1", RpcTarget.All, x.ToString());
                }
            }
            else if (hit.transform.tag == "tableau2")
            {
                Debug.Log(dragging);
                if (dragging)
                {
                    float x = Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
                    pv.RPC("Mtableau2", RpcTarget.All, x.ToString());
                }
            }
            else if (hit.transform.tag == "zarbiya")
            {
                Debug.Log(dragging);
                if (dragging)
                {
                    float x = Input.GetAxis("Mouse X") * translationSpeed * Time.deltaTime;
                    pv.RPC("Mzarbiya", RpcTarget.All, x.ToString());
                }
            }
            else if (hit.transform.tag == "korsi")
            {
                Debug.Log(dragging);
                if (dragging)
                {
                    float x = Input.GetAxis("Mouse X") * translationSpeed * Time.deltaTime;
                    pv.RPC("Mkorsi", RpcTarget.All, x.ToString());
                }
            }
            else if (hit.transform.tag == "sabela")
            {
                if (Input.GetMouseButtonUp(0))
                {

                    pv.RPC("PTap", RpcTarget.All);
                  
                }
            }
            else if ((hit.transform.tag == "sabela2") & (sabela1))
            {
                if (Input.GetMouseButtonUp(0))
                {

                    pv.RPC("PTap1", RpcTarget.All);

                }
            }
            else if ((hit.transform.tag == "sabela3") & sabela2)
            {
                if (Input.GetMouseButtonUp(0))
                {

                    pv.RPC("PTap2", RpcTarget.All);

                }
            }
            
        }
        if (inc == 0)
        {
            pv.RPC("D2", RpcTarget.All);
        }

    }
    void Move()
    {
        Vector3 moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        moveAmount = Vector3.SmoothDamp(moveAmount, moveDir * (Input.GetKey(KeyCode.LeftShift) ? sprintSpeed : walkSpeed), ref smoothMoveVelocity, smoothTime);
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        anim.SetFloat("moveX", x);
        anim.SetFloat("moveY", z);
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            anim.SetTrigger("reach");

        }
        if (Input.GetKeyDown(KeyCode.C) && anim.GetBool("iscrouch") == false)
        {
            anim.SetTrigger("crouch");
            anim.SetBool("iscrouch", true);
        }
        else if (Input.GetKeyDown(KeyCode.C) && anim.GetBool("iscrouch") == true)
        {
            anim.SetBool("iscrouch", false);
        }
    }
    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + transform.TransformDirection(moveAmount) * Time.fixedDeltaTime);
    }

    [PunRPC]
    public void bookOut()
    {
        anime.SetBool("state", true);
        anime.SetTrigger("out");
    }
    [PunRPC]
    public void bookIn()
    {
        anime.SetBool("state", false);
        anime.SetTrigger("in");
    }
    [PunRPC]
    public void Mgarn(string x)
    {
       
        garn.transform.Rotate(Vector3.back * float.Parse(x));
    }
    [PunRPC]
    public void Mgarn1(string x)
    {
   
        garn1.transform.Rotate(Vector3.back * float.Parse(x));
    }
    [PunRPC]
    public void Mtableau(string x)
    {

        tableau.transform.Rotate(Vector3.down * float.Parse(x));
    }
    [PunRPC]
    public void Mtableau1(string x)
    {
  
        tableau1.transform.Rotate(Vector3.down * float.Parse(x));
    }
    [PunRPC]
    public void Mtableau2(string x)
    {

        tableau2.transform.Rotate(Vector3.down * float.Parse(x));
    }

    [PunRPC]
    public void PTap()
    {
        water.Play();
        inc = 2;
        sabela1 = true;

    }
    [PunRPC]
    public void PTap1()
    {
        water2.Play();
        inc = 1;
        sabela2 = true;

    }
    [PunRPC]
    public void PTap2()
    {
        water3.Play();
        inc = 0;
 

    }
    [PunRPC]
    public void D2()
    {
        Destroy(GameObject.FindWithTag("beb2"));
    }
    [PunRPC]
    public void Mkorsi(string x)
    {

        korsi.transform.Translate(Vector3.back * float.Parse(x));
    }
    [PunRPC]
    public void Mzarbiya(string x)
    {

        zarbiya.transform.Translate(Vector3.forward * float.Parse(x)*2);
    }



}

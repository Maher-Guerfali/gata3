using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class RestApis : MonoBehaviour
{
    private string email;
    private string password;
    public string username;
    [SerializeField]
    public GameObject registerPanel;
    [SerializeField]
    public Text userText;

    [SerializeField]
    public GameObject LoginPanel;

    [SerializeField]
    public GameObject MainMenu;

    private class User
    {
        public string Email;
        public string Password;
    }
    private class SetUser
    {
        public string Email;
        public string Username;
        public string Password;
    }
    

    IEnumerator Login()
    {
        User user = new User();
        user.Email = email;
        user.Password = password;
        string jsonBody = JsonUtility.ToJson(user);
        byte[] bytes = Encoding.UTF8.GetBytes(jsonBody);



        UnityWebRequest www = new UnityWebRequest("http://localhost:3001/api/user/login", "POST");
        www.SetRequestHeader("Content-Type", "application/json");
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bytes);
        www.downloadHandler = new DownloadHandlerBuffer();


        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            if (www.responseCode == 404)
            {
                Debug.Log("user not found");
            }
            else if (www.responseCode == 400)
            {
                Debug.Log("wrong Password");
            }
            else
            {

                Debug.Log(www.downloadHandler.text);
            }
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            string username = www.downloadHandler.text.Substring(www.downloadHandler.text.IndexOf("Username")+11);
            username = username.Substring(0, username.IndexOf(",")-1);
            PlayerPrefs.SetString("NickName", username);
            if (username.Length>0)
            {
             PhotonNetwork.NickName = username;

            }
           
            userText.text = username;


            LoginPanel.SetActive(false);
             MainMenu.SetActive(true);
        }

    }
    IEnumerator Register()
    {
        SetUser user = new SetUser();
        user.Email = email;
        user.Password = password;
        user.Username = username;
        string jsonBody = JsonUtility.ToJson(user);
        byte[] bytes = Encoding.UTF8.GetBytes(jsonBody);



        UnityWebRequest www = new UnityWebRequest("http://localhost:3001/api/user/register", "POST");
        www.SetRequestHeader("Content-Type", "application/json");
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bytes);
        www.downloadHandler = new DownloadHandlerBuffer();


        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            if (www.responseCode == 400)
            {
                Debug.Log("user already exist");
            }
            else
            {
               Debug.Log(www.downloadHandler.text);
              
            }

           

        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            LoginPanel.SetActive(true);
            registerPanel.SetActive(false);
        }

    }

    public void LoginClick()
    {
        StartCoroutine(Login());
    }

    public void GetEmail(string emailIn)
    {
        email = emailIn;
    }
    public void GetPassword(string passwordIn)
    {
        password = passwordIn;
    }
    public void RegisterClick()
    {
        StartCoroutine(Register());
    }

    public void SetEmail(string emailIn)
    {
        email = emailIn;
    }
    public void SetPassword(string passwordIn)
    {
        password = passwordIn;
    }
    public void SetUsername(string usernameIn)
    {
        username = usernameIn;
    }

   
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;


public class CustomMatchMakingLobbyController : MonoBehaviourPunCallbacks
{

    [SerializeField]
    private GameObject lobbyCConnectButton;
    [SerializeField]
    private GameObject watchButton;
    [SerializeField]
    private GameObject lobbyPanel;
    [SerializeField]
    private GameObject mainPanel;
    [SerializeField]
    private GameObject nameInput;
    [SerializeField]
    private GameObject createButton;
    [SerializeField]
    private Text username;

    private static string nickname;
    private bool watcher;
    private string roomName;
    private int roomSize = 4;

    private List<RoomInfo> roomListings;
    [SerializeField]
    private Transform roomContainer;
    [SerializeField]
    private GameObject roomListingPrefab;

    public override void OnConnectedToMaster()
    {

        PhotonNetwork.AutomaticallySyncScene = true;
        watcher = false;
        lobbyCConnectButton.SetActive(true);
        watchButton.SetActive(true);
        roomListings = new List<RoomInfo>();

        if (PlayerPrefs.HasKey("NickName"))
        {
            if(PlayerPrefs.GetString("NickName") == "")
            {
                PhotonNetwork.NickName = "Player " + Random.Range(0, 1000);
            }
            else
            {
                PhotonNetwork.NickName = PlayerPrefs.GetString("NickName");
             

            }
        }
        else
        {
            PhotonNetwork.NickName = "Player " + Random.Range(0, 1000);
        }
      

    }

    public void PlayerNameUpdate(string nameInput)
    {
        PhotonNetwork.NickName = nameInput;
        PlayerPrefs.SetString("NickName", nameInput);
    }
    public void JoinLobbyOnClick()
    {
        mainPanel.SetActive(false);
        lobbyPanel.SetActive(true);
        nameInput.SetActive(true);
        createButton.SetActive(true);
        PhotonNetwork.JoinLobby();
    }
    public void JoinLobbyOnClickAsWatcher()
    {
        mainPanel.SetActive(false);
        lobbyPanel.SetActive(true);
        nameInput.SetActive(false);
        createButton.SetActive(false);
        nickname = PhotonNetwork.NickName;
        Debug.Log(nickname);
        PhotonNetwork.NickName = "Watcher" + Random.Range(0, 1000);
        watcher = true;
        PhotonNetwork.JoinLobby();
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        int tempIndex;
        foreach(RoomInfo room in roomList)
        {
            if(roomListings!= null)
            {
                tempIndex = roomListings.FindIndex(ByName(room.Name));
            }
            else
            {
                tempIndex = -1;
            }
            if(tempIndex != -1)
            {
            
                roomListings.RemoveAt(tempIndex);
                Destroy(roomContainer.GetChild(tempIndex).gameObject);

            }
            if (room.PlayerCount > 0)
            {
                roomListings.Add(room);
                ListRoom(room);
            }
        }

    }
    static System.Predicate<RoomInfo> ByName(string name)
    {
        return delegate (RoomInfo room)
        {
            return room.Name == name;
        };
    }

    void ListRoom (RoomInfo room)
    {
        if(room.IsOpen && room.IsVisible)
        {
            GameObject tempListing = Instantiate(roomListingPrefab, roomContainer);
            RoomButton tempbutton = tempListing.GetComponent<RoomButton>();
            tempbutton.SetRoom(room.Name, room.MaxPlayers, room.PlayerCount);
        }
        if(room.IsOpen == false && watcher)
        {
            GameObject tempListing = Instantiate(roomListingPrefab, roomContainer);
            RoomButton tempbutton = tempListing.GetComponent<RoomButton>();
            tempbutton.SetRoom(room.Name, room.MaxPlayers, room.PlayerCount);
        }
    }

    public void onRoomNameChanged(string nameIn)
    {
        roomName = nameIn;
    }

    public void CreateRoom()
    {
        Debug.Log("Creating Room Now");
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)roomSize };
        PhotonNetwork.CreateRoom(roomName, roomOps); 

    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a new room but failed, there must already be a room with the same name");

    }
    public void MatchMakingCancel()
    {
        mainPanel.SetActive(true);
        watcher = false;
        PhotonNetwork.NickName = nickname;
        Debug.Log(PhotonNetwork.NickName);
        lobbyPanel.SetActive(false);
        PhotonNetwork.LeaveLobby();
    }



}

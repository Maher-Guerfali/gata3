using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class CustomMatchMakingRoomController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    public static int multiPlayerSceneIndex;

    [SerializeField]
    private GameObject lobbyPanel;
    [SerializeField]
    private GameObject roomPanel;

    [SerializeField]
    private GameObject startButton;

    [SerializeField]
    private Transform playercontainer;
    [SerializeField]
    private GameObject playerListingPrefab;

    [SerializeField]
    private Text roomNameDisplay;


    [SerializeField]
    private GameObject chooseRoom;


    void ClearPlayerListings()
    {
        for(int i = playercontainer.childCount -1; i >= 0; i--)
        {
            Destroy(playercontainer.GetChild(i).gameObject);
        }
    }
    void ListPlayers()
    {
        foreach(Player player in PhotonNetwork.PlayerList)
        {
            if(player.NickName.Contains("Watcher")==false)
            {
                GameObject tempListing = Instantiate(playerListingPrefab, playercontainer);

                Text tempText = tempListing.transform.GetChild(0).GetComponent<Text>();
                tempText.text = player.NickName;
            }
          
        }
    }
    public override void OnJoinedRoom()
    {
        roomPanel.SetActive(true);
        lobbyPanel.SetActive(false);
        roomNameDisplay.text = PhotonNetwork.CurrentRoom.Name;
        if (PhotonNetwork.IsMasterClient)
        {
            startButton.SetActive(true);

        }
        else
        {
            startButton.SetActive(false);
        }
        ClearPlayerListings();
        ListPlayers();
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        ClearPlayerListings();
        ListPlayers();
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        ClearPlayerListings();
        ListPlayers();
        if (PhotonNetwork.IsMasterClient)
        {
            startButton.SetActive(true);
        }
    }

    public void StartGame()
    {

        if (PhotonNetwork.IsMasterClient)
        {
            chooseRoom.SetActive(true);
            
            roomPanel.SetActive(false);
     
        }
    }
    public void EnterRoom()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.LoadLevel(multiPlayerSceneIndex);
        }
       

    }

    public void Room1()
    {
        multiPlayerSceneIndex = 3;

 
   
    }
    public void Room2()
    {
        multiPlayerSceneIndex = 4;

    }
    IEnumerable rejoinLobby()
    {
        yield return new WaitForSeconds(1);
        PhotonNetwork.JoinLobby();
    }

    public void BackOnClick()
    {
        lobbyPanel.SetActive(true);
        roomPanel.SetActive(false);
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LeaveLobby();
        StartCoroutine("rejoinLobby");
    }
}

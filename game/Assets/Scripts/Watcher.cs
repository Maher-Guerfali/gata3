using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Watcher : MonoBehaviourPunCallbacks

{
    [SerializeField]
    float mouseXSensitivity;
    float verticalLookRotation;
    [SerializeField] GameObject cameraHolder;




    GameObject[] players;
    private PhotonView PV;
    

    private int j;


    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();

        j = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (PV.IsMine)
        {
            Look();
            players = GameObject.FindGameObjectsWithTag("Player");
            if(players.Length != 0)
            {
                transform.position = players[j].transform.position;
                if (Input.GetKeyDown(KeyCode.Mouse0)){
                    if(j >= players.Length - 1)
                    {
                        j = 0;
                    }
                    else
                    {
                        j = j+ 1 ;
                    }
                }
            }
            


        }
        else
        {
            cameraHolder.gameObject.SetActive(false);
        }

    }
    void Look()
    {
        transform.Rotate(Vector3.up * Input.GetAxisRaw("Mouse X") * mouseXSensitivity);
        verticalLookRotation += Input.GetAxisRaw("Mouse Y") * mouseXSensitivity;
        verticalLookRotation = Mathf.Clamp(verticalLookRotation, -90f, 90f);
        cameraHolder.transform.localEulerAngles = Vector3.left * verticalLookRotation;
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            cameraHolder.GetComponentInChildren<Camera>().fieldOfView--;

        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            cameraHolder.GetComponentInChildren<Camera>().fieldOfView++;

        }
        if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            cameraHolder.GetComponentInChildren<Camera>().fieldOfView = 60;

        }

    }
}

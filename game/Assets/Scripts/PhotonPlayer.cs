using Photon.Pun;
using Photon.Voice.Unity;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PhotonPlayer : MonoBehaviour
{

    private PhotonView PV;
    public GameObject myAvatar;
 

    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();
        int spawnPicker = Random.Range(0, GameSetups.GS.spawnPoints.Length);


        if (PV.IsMine)
        {
           myAvatar= PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs","PhotonPlayer"),
                GameSetups.GS.spawnPoints[spawnPicker].position, GameSetups.GS.spawnPoints[spawnPicker].rotation,0);
            transform.position = myAvatar.transform.position;

        }


    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 100f;
    bool dragging = false;
    GameObject rb;
    private void Start()
    {
       
    }
    private void OnMouseDrag()
    {
        dragging = true;
    }
    private void Update()
    {
         if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
        }
    }
    private void FixedUpdate()
    {
        if (dragging)
        {
            float x = Input.GetAxis("Mouse X") * rotationSpeed * Time.fixedDeltaTime;
            rb.transform.Rotate(Vector3.back * x);
        }
    }
}

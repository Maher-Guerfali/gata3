using ExitGames.Client.Photon;
using Photon.Chat;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotonChatManger : MonoBehaviour,IChatClientListener
{

    ChatClient chatClient;

    [SerializeField]
    GameObject ChatPanel;
    [SerializeField]
    InputField ChatInput;

    [SerializeField]
    Text ChatDisplay;

    private string msg;
    private string to;


    public void DebugReturn(DebugLevel level, string message)
    {
      
    }

    public void OnChatStateChange(ChatState state)
    {
     
    }

    public void OnConnected()
    {
      
        chatClient.Subscribe(new string[] { "channelA" });
        
    }

    public void OnDisconnected()
    {
     
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {

        
     
        string temp = "";
        for(int i = 0; i < senders.Length; i++)
        {
            temp = senders[i] + ": " + messages[i];
        }
        ChatDisplay.text += temp + "\n";
    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        string temp = "";
        temp = "(Private) " + sender + ": " + message;
        ChatDisplay.text += temp + "\n";

    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
     
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        ChatPanel.SetActive(true);
        chatClient.PublishMessage("channelA", "Connceted to chat");

    }

    public void OnUnsubscribed(string[] channels)
    {
       
    }

    public void OnUserSubscribed(string channel, string user)
    {
     
    }

    public void OnUserUnsubscribed(string channel, string user)
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.LocalPlayer.NickName.Contains("Watcher") == false)
        {
            chatClient = new ChatClient(this);

            chatClient.ChatRegion = "EU";
            chatClient.Connect(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat, PhotonNetwork.AppVersion, new AuthenticationValues(PhotonNetwork.NickName));
            msg = "";
            to = "";
        }
            
  


    }

    // Update is called once per frame
    void Update()
    {

        if (PhotonNetwork.LocalPlayer.NickName.Contains("Watcher") == false)
        {
            chatClient.Service();

            if (Input.GetKeyDown(KeyCode.I) && ChatInput.isFocused == false)
            {
                ChatInput.ActivateInputField();

            }
            if (ChatInput.isFocused)
            {
                if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
                {
                    if (msg.Contains("/"))
                    {
                        int f = msg.IndexOf("/");
                        int l = msg.IndexOf(" ");
                        to = msg.Substring(f + 1, l - f - 1);
                        msg = msg.Substring(l + 1);

                    }
                    else
                    {
                        to = "";
                    }
                    Debug.Log("to: " + to + " msg: " + msg);
                    SendText();
                }
            }
        }
        
       
        
    }

    public void ChatText()
    {
        msg = ChatInput.text;
        
    }
    
    public void SendText()
    {
        if (to == "" && msg!="")
        {
          
            chatClient.PublishMessage("channelA", msg);
            ChatInput.text = "";
            msg = "";
            ChatInput.DeactivateInputField();
        }
        else if (msg != "" && to != "")
        {
            chatClient.SendPrivateMessage(to, msg);
            ChatInput.text = "";
            msg = "";
            to = ""; 
            ChatInput.DeactivateInputField();
        }
    }
   
}

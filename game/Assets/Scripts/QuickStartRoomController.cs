using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class QuickStartRoomController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private int waitingRoomSceneIndex;

    public override void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);

    }
    public override void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public override void OnCreatedRoom()
    {
        if (PhotonNetwork.CurrentRoom.Name.Contains("Random Room"))
        {
            Debug.Log("Joined Room");
            SceneManager.LoadScene(waitingRoomSceneIndex);
        }
    }
   
    
}

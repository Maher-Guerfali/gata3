using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PhotonRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    public static PhotonRoom room;
    private PhotonView PV;
    public bool isGameLoaded;
    public int currentScene;

    private Player[] photonPlayers;
    public int myNumberInRoom;
    public int playerInRoom;
    public int playerInGame;
    private int watcherNumber;
    public Text time; 

    private bool readyToCount;
    private bool readyToStart;
    public float startingTime;
    private float lessThanMaxPlayers;
    private float atMaxPlayers;
    private float timeToStart;

    private void Awake()
    {
        if (PhotonRoom.room == null)
        {
            PhotonRoom.room = this;

        }
        else
        {
            if (PhotonRoom.room != this)
            {
                Destroy(PhotonRoom.room.gameObject);
                PhotonRoom.room = this;
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        PhotonNetwork.LoadLevel(0);
    }
    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }
    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }

    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();
        readyToCount = false;
        readyToStart = false;
        lessThanMaxPlayers = startingTime;
        atMaxPlayers = 6;
        watcherNumber = 0;
        timeToStart = startingTime;
    

    }

    // Update is called once per frame
    void Update()
    {
   
        if (MultiplayerSettings.multiplayerSettings.delayStart)
        {
            if (playerInRoom == 1)
            {
                RestartTimer();

            }
            if (!isGameLoaded)
            {
                if (readyToStart)
                {
                    atMaxPlayers -= Time.deltaTime;
                    lessThanMaxPlayers = atMaxPlayers;
                    timeToStart = atMaxPlayers;

                } else if (readyToCount)
                {
                    lessThanMaxPlayers -= Time.deltaTime;
                    timeToStart = lessThanMaxPlayers;
                }
             
                if (timeToStart <= 0)
                {
                    StartGame();
                }
            }
        }
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("Joined Room");
        photonPlayers = PhotonNetwork.PlayerList;
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (player.NickName.Contains("Watcher"))
            {
                watcherNumber++;
            }

        }
        playerInRoom = photonPlayers.Length - watcherNumber;
        myNumberInRoom = playerInRoom;
  

        if (MultiplayerSettings.multiplayerSettings.delayStart)
        {
            Debug.Log("players in room out of max players possible (" + playerInRoom + ":" + MultiplayerSettings.multiplayerSettings.maxPlayers + ")");
            if (playerInRoom > 1)
            {
                readyToCount = true;

            } if (playerInRoom == MultiplayerSettings.multiplayerSettings.maxPlayers)
            {
                readyToStart = true;
                if (!PhotonNetwork.IsMasterClient)
                    return;
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }

        }
        else
        {
            StartGame();
        }

    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("a new player has joined the room");
        photonPlayers = PhotonNetwork.PlayerList;
        playerInRoom++;
        if (MultiplayerSettings.multiplayerSettings.delayStart)
        {
            Debug.Log("players in room out of max players possible (" + playerInRoom + ":" + MultiplayerSettings.multiplayerSettings.maxPlayers + ")");
            if (playerInRoom >= MultiplayerSettings.multiplayerSettings.maxPlayers  )
            {
                readyToCount = true;
            }
            if (playerInRoom == MultiplayerSettings.multiplayerSettings.maxPlayers)
            {
                readyToStart = true;
                if (!PhotonNetwork.IsMasterClient)
                    return;
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }

        }
    }
    void StartGame()
    {
        isGameLoaded = true;
        if (!PhotonNetwork.IsMasterClient)
            return;
        if (MultiplayerSettings.multiplayerSettings.delayStart)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;

        }
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.LoadLevel(CustomMatchMakingRoomController.multiPlayerSceneIndex);
    }
    void RestartTimer()
    {
        lessThanMaxPlayers = startingTime;
        timeToStart = startingTime;
        atMaxPlayers = 6;
        readyToCount = false;
        readyToStart = false;
    }
    void OnSceneFinishedLoading(Scene scene , LoadSceneMode mode)
    {
       
        currentScene = scene.buildIndex;
        if(currentScene == 3 || currentScene==4)
        {
            isGameLoaded = true;
            if (MultiplayerSettings.multiplayerSettings.delayStart)
            {
                PV.RPC("RPC_LoadedGameScene",RpcTarget.MasterClient);
            }
            else
            {
                RPC_CreatePlayer();
            }
        }
    }

    [PunRPC]
    private void RPC_LoadedGameScene()
    {
        playerInGame++;
        if(playerInGame == PhotonNetwork.PlayerList.Length - watcherNumber)
        {
            PV.RPC("RPC_CreatePlayer", RpcTarget.All);
        }
    }

    [PunRPC]
    private void RPC_CreatePlayer()
    {
        if (PhotonNetwork.LocalPlayer.NickName.Contains("Watcher") == false)
        {
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonAvatar"), transform.position, Quaternion.identity, 0);
        }
        else
        {
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonObserver"), transform.position, Quaternion.identity, 0);
        }
        
    }

}

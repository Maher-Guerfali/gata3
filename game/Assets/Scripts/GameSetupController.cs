using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using System;
using Photon.Realtime;

public class GameSetupController : MonoBehaviourPunCallbacks
{

    private void Awake()
    {
      
    }

    // Start is called before the first frame update
    void Start()
    {
        CreatePlayer();
   
    }

    private void CreatePlayer()
    {
        Debug.Log("Creating Player");
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonAvatar"), Vector3.forward * UnityEngine.Random.Range(0,3), Quaternion.identity);
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    
            
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Destroy(PhotonRoom.room.gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class KeypadController : MonoBehaviour
{
    public DoorController door;
    public string password;
    public int m; 
    public int passwordLimit;
    public Text passwordText;
    
    

    [Header("Audio")]
    public AudioSource audioSource;
    public AudioClip correctSound;
    public AudioClip wrongSound;
    PhotonView pv;
    [SerializeField]
    private GameObject youwin;


   
       

        private void Start()
    {
        if (gameObject.GetComponent<PhotonView>())
        {
            pv = gameObject.GetComponent<PhotonView>();
        }
        passwordText.text = "";
        youwin.SetActive(false);
    }

    public void PasswordEntry(string number)
    {
        if (number == "Clear")
        {
            Clear();
            return;
        }
        else if(number == "Enter")
        {
            Enter();
            return;
        }

        int length = passwordText.text.ToString().Length;
        if(length<passwordLimit)
        {
            passwordText.text = passwordText.text + number;
        }
    }

    public void Clear()
    {
        passwordText.text = "";
        passwordText.color = Color.white;
    }

    private void Enter()
    {
        if (passwordText.text == password)
        {

            pv.RPC("RemoveDoor", RpcTarget.All);

            
        }
        
       
    }

    IEnumerator waitAndClear()
    {
        yield return new WaitForSeconds(0.75f);
        Clear();
    }

    [PunRPC]
    public void RemoveDoor()
    {
        youwin.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Invoke("BacktoMenu", 10);
    }

    public void BacktoMenu()
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(0);
    }


}

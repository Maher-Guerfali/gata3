using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    GameObject Play,Login, Main;

    void Start()
    {
        Play = GameObject.Find("PlayMenu");
        Main = GameObject.Find("MainMenu");
        Login = GameObject.Find("LoginMenu");
        Main.SetActive(true);
    }
    public void play()
    {
        SceneManager.LoadScene("PlayMenu");
        Play.SetActive(true);
        Main.SetActive(false);
        Login.SetActive(false);
    } 

    public void Quit()
    {
        Application.Quit();
    }

}
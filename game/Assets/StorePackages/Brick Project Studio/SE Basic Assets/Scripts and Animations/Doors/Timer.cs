using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviourPunCallbacks
{
    public float timeLeft;
    Text txt;
    GameObject vp;
    public GameObject vs;

    // Start is called before the first frame update
    void Start()
    {
        txt = GetComponent<Text>();
        vp = GameObject.Find("GameOver");
        vp.SetActive(false);
       
        
    }

    // Update is called once per frame
    void Update()
    {
        int time= (int)timeLeft;
        txt.text = time.ToString();
        timeLeft -= Time.deltaTime;
        
            if (timeLeft <= 0)
            {
            vs.GetComponent<Canvas>().enabled = false;
             vp.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Invoke("GameOver",5);
            }
        
    }

    public void GameOver()
    {


            PhotonNetwork.LeaveRoom();
            SceneManager.LoadScene(0);
            
        
    }
}

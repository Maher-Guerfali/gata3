﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SojaExiles

{
    public class opencloseDoor : MonoBehaviour
    {

        public Animator openandclose;

        PhotonView pv;


        void Start()
        {
            if (gameObject.GetComponent<PhotonView>())
            {
                pv = gameObject.GetComponent<PhotonView>();
            }
        }

        void OnMouseOver()
        {




            if (openandclose.GetBool("opening"))
            {
                if ((Input.GetMouseButtonDown(0)))
                {
                    pv.RPC("open", RpcTarget.All);
                }
            }
            else
            {

                if (Input.GetMouseButtonDown(0))
                {
                    pv.RPC("close", RpcTarget.All);
                }


            }






        }
        [PunRPC]
        public void open()
        {
            openandclose.SetBool("opening", false);
            openandclose.SetTrigger("open");
        }
        [PunRPC]
        public void close()
        {
            openandclose.SetBool("opening", true);
            openandclose.SetTrigger("close");
        }




    }
}
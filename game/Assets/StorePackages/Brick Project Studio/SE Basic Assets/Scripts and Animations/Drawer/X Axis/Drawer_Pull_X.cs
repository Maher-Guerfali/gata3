﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SojaExiles

{

	public class Drawer_Pull_X : MonoBehaviour
	{

		public Animator pull_01;
		
	
		PhotonView pv;

		void Start()
		{
			
            if (gameObject.GetComponent<PhotonView>()){
	pv = gameObject.GetComponent<PhotonView>();
            }
		
		}

		void OnMouseOver()
		{
			
				
				  
					
				
						if (pull_01.GetBool("opening"))
						{
							if (Input.GetMouseButtonDown(0))
							{
					pv.RPC("open", RpcTarget.All);
							}
						}
						else
						{
						
								if (Input.GetMouseButtonDown(0))
								{
					pv.RPC("close", RpcTarget.All);
				}
							

						}

					
				

			

		}

		IEnumerator opening()
		{
			print("you are opening the door");
			pull_01.Play("openpull_01");
			pull_01.SetBool("opening", false);
			yield return new WaitForSeconds(.5f);
		}

		IEnumerator closing()
		{
			print("you are closing the door");
			pull_01.Play("closepush_01");
			pull_01.SetBool("opening", true);
			yield return new WaitForSeconds(.5f);
		}
		[PunRPC]
		public void open()
        {
			pull_01.SetBool("opening", false);
			pull_01.SetTrigger("open");
		}
		[PunRPC]
		public void close()
		{
			pull_01.SetBool("opening", true);
			pull_01.SetTrigger("close");
		}

	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SojaExiles

{
    public class MouseLook : MonoBehaviour
    {

        public float mouseXSensitivity;

        public Transform playerBody;
        public Transform cursor;
        public float rayhit;
        RawImage item;
        float xRotation = 0f;
        int i;
        public bool haskey;
        Texture2D key;

        private GameObject gameobject;
        Animation anim;

        // Start is called before the first frame update
        void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            i = 0;
            item = GameObject.FindGameObjectWithTag("Item").GetComponent<RawImage>();
            haskey = false;
            key = Resources.Load<Texture2D>("Key");
        }

        // Update is called once per frame
        void Update()
        {
            /*RaycastHit hit;
            Debug.DrawRay(transform.position,transform.forward * rayhit, Color.red); 
            if(Physics.Raycast(transform.position, transform.forward ,out hit, rayhit))
            {
                if (hit.transform.tag == "Interactble")
                {
                    gameobject = hit.transform.gameObject;
                    Renderer renderer = gameobject.GetComponent<Renderer>();
                    
                    renderer.material.color = Color.yellow;

                }
                else
                {
                   if(gameobject != null)
                    {
                        Renderer renderer = gameobject.GetComponent<Renderer>();
                        renderer.material.color = Color.white;
                    }
                }
            }*/

            float mouseX = Input.GetAxis("Mouse X") * mouseXSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseXSensitivity * Time.deltaTime;
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                GetComponent<Camera>().fieldOfView--;
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                GetComponent<Camera>().fieldOfView++;
            }
            if (Input.GetKeyDown(KeyCode.Mouse2))
            {
                GetComponent<Camera>().fieldOfView = 60;
            }

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            playerBody.Rotate(Vector3.up * mouseX);

            RaycastHit hit;
            Debug.DrawRay(transform.position, transform.forward * rayhit, Color.red);
            if (Physics.Raycast(transform.position, transform.forward, out hit, rayhit))
            {
                if (hit.transform.tag == "Book")
                {

                    anim = hit.transform.GetComponent<Animation>();
                    if (Input.GetKeyDown(KeyCode.Mouse0) && i % 2 == 0)
                    {
                        anim.Play("Book 1");
                        i++;

                    }
                    else if (Input.GetKeyDown(KeyCode.Mouse0) && i % 2 != 0)
                    {
                        anim.Play("Book");
                        i++;

                    }

                }
                else if (hit.transform.tag == "Key")
                {

                    if (Input.GetKeyDown(KeyCode.Mouse0))
                    {
                        Destroy(hit.transform.gameObject);
                        item.enabled = true;
                        haskey = true;
                        item.texture = key;



                    }
                }
            }

        }
    }
}
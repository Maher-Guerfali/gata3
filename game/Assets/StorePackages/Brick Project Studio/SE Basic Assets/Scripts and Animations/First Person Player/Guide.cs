using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide : MonoBehaviour
{ public GameObject Forward, Back, Left, Right,Interact;
    bool doneW = false;
    bool doneA = false;
    bool doneS = false;
    bool doneD = false;
    bool doneMouse = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.W))&&(doneW==false))
            {
            Forward.SetActive(false);
            Left.SetActive(true);
            doneW = true;
        }
        if ((Input.GetKeyDown(KeyCode.A))&& (doneA == false))
        {
            Left.SetActive(false);
            Right.SetActive(true);
            doneA = true;

        }
        if ((Input.GetKeyDown(KeyCode.D))&& (doneD == false))
        {
            Right.SetActive(false);
            Back.SetActive(true);
            doneD = true;

        }
        if ((Input.GetKeyDown(KeyCode.S)) && (doneS == false))
        {
            Back.SetActive(false);
            Interact.SetActive(true);
            doneS = true;

        }
        if ((Input.GetMouseButtonDown(0)) && (doneMouse == false))
        {
            Interact.SetActive(false);

            doneMouse = true;

        }
    }
}

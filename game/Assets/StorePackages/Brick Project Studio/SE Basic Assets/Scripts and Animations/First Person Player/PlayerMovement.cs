﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SojaExiles

{
    public class PlayerMovement : MonoBehaviour
    {

        public CharacterController controller;
        public Animator anim;
        public float speed ;
        public float gravity ;

        Vector3 velocity;

        bool isGrounded;

        // Update is called once per frame
        void Update()
        {

            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            anim.SetFloat("moveX", x);
            anim.SetFloat("moveY", z);
            Vector3 move = transform.right * x + transform.forward * z;

            controller.Move(move * speed * Time.deltaTime);

            velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                anim.SetTrigger("reach");
            }
            if (Input.GetKeyDown(KeyCode.C) && anim.GetBool("iscrouch") == false)
            {
                anim.SetTrigger("crouch");
                anim.SetBool("iscrouch", true);
            }else if (Input.GetKeyDown(KeyCode.C) && anim.GetBool("iscrouch") == true)
            {
                anim.SetBool("iscrouch", false);
            }
         
            
            

        }
    }
}